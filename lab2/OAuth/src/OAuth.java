import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;

import com.temboo.core.TembooException;

public class OAuth
{
	/**
	 * Parse Google Calendar's JSON string and print the
	 * date/time/title
	 * @param eventJSON
	 */
	public static void parse(String eventJSON) {
		try
		{
			// Save the events string as a JSON object
			JSONObject json = new JSONObject(eventJSON);
		
			// Get the JSON array named "items"
			JSONArray events = (JSONArray) json.get("items");
			
            // Iterate through each element within events
			for(int i = 0; i < events.length(); i++) {
				JSONObject item = (JSONObject) events.get(i);

				// Print the title of the event
				System.out.println("Event Title: " + item.get("summary"));

				// Grab the date strings
				String startDateAndTime  = item.getJSONObject("start").getString("dateTime"); 
				String endDateAndTime  = item.getJSONObject("end").getString("dateTime"); 

				// Define the input and output date string format
				SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
				SimpleDateFormat output = new SimpleDateFormat("MM/dd/yyyy hh:mm a");

				// Parse the start and end date strings
				Date sdt = input.parse(startDateAndTime);
				Date edt = input.parse(endDateAndTime);

				// Print the formatted start and end date string
				System.out.println("Start: " + output.format(sdt));
				System.out.println("End: " + output.format(edt));
				System.out.println();

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws TembooException, IOException, URISyntaxException, InterruptedException
	{
		OAuthInitialize iniOAuth = new OAuthInitialize();
		String callbackID = iniOAuth.getCallbackID();

		/**
		 * Open the browser to allow the user to authorize using
		 * Google Authentication
		 */
		System.out.println("Copy this URL and paste it to your browser:");
		System.out.println();
		System.out.println(iniOAuth.getAuthURL());
		System.out.println();

		/**
		 * Wait for user input before continuing to the finalize
		 * OAuth procedure
		 */
		System.out.println("Press any key after you've authorized to continue.");
		Scanner scanner = new Scanner(System.in);
		String devNull = scanner.nextLine();

		// Initialize the finalize authorization object
		OAuthFinalize finOAuth = new OAuthFinalize(callbackID);

		// Get the first calendar using the refresh token generate by
		// finalize authorization
		GetCalendars myCalender = new GetCalendars(finOAuth.getRefreshToken());

		// Parse the Google Calendar response
		OAuth.parse(myCalender.getAllEventsResults().get_Response());
	}
}
