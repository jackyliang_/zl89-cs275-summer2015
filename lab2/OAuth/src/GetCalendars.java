import com.temboo.Library.Google.Calendar.GetAllCalendars;
import com.temboo.Library.Google.Calendar.GetAllCalendars.GetAllCalendarsInputSet;
import com.temboo.Library.Google.Calendar.GetAllCalendars.GetAllCalendarsResultSet;
import com.temboo.Library.Google.Calendar.GetAllEvents;
import com.temboo.Library.Google.Calendar.GetAllEvents.GetAllEventsInputSet;
import com.temboo.Library.Google.Calendar.GetAllEvents.GetAllEventsResultSet;
import com.temboo.Library.Google.OAuth.*;
import com.temboo.Library.Google.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Google.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;



public class GetCalendars
{
	private TembooSession session;
    private GetAllCalendars getAllCalendarsChoreo;
    private GetAllCalendarsInputSet getAllCalendarsInputs;
	
    /**
     * Get the first calendar 
     * @param refreshToken
     * @throws TembooException
     */
	public GetCalendars(String refreshToken) throws TembooException {
		// Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
		session = new TembooSession("faroskalin", "myFirstApp", "39e34c8c02b444879db847e11ebad224");

		getAllCalendarsChoreo = new GetAllCalendars(session);

		// Get an InputSet object for the choreo
		getAllCalendarsInputs = getAllCalendarsChoreo.newInputSet();

		// Set inputs
		getAllCalendarsInputs.set_ClientSecret("a2hPHwlrQmVPJXTNRN69YOE1");
		getAllCalendarsInputs.set_RefreshToken(refreshToken);
		getAllCalendarsInputs.set_ClientID("446983882758-t927vcfnmuchafclb7dcrapt2mu0devf.apps.googleusercontent.com");
	}

	/**
	 * Get our calendar results
	 * @return
	 * @throws TembooException
	 */
	public GetAllCalendarsResultSet getAllCalendars() throws TembooException {
		GetAllCalendarsResultSet getAllCalendarsResults = getAllCalendarsChoreo.execute(getAllCalendarsInputs);	
		return getAllCalendarsResults;
	}
	
	/**
	 * Get all events within the calendar
	 * @return
	 * @throws TembooException
	 */
	public GetAllEventsResultSet getAllEventsResults() throws TembooException {
		GetAllEvents getAllEventsChoreo = new GetAllEvents(session);

		// Get an InputSet object for the choreo
		GetAllEventsInputSet getAllEventsInputs = getAllEventsChoreo.newInputSet();

		// Set credential to use for execution
		getAllEventsInputs.setCredential("GetAllEvents");

		// Execute Choreo
		GetAllEventsResultSet getAllEventsResults = getAllEventsChoreo.execute(getAllEventsInputs);	

		return getAllEventsResults;
	}
}
