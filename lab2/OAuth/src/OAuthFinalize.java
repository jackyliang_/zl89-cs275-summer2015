import java.util.Scanner;

import com.temboo.Library.Google.OAuth.FinalizeOAuth;
import com.temboo.Library.Google.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Google.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;


public class OAuthFinalize
{
	private FinalizeOAuthResultSet finalizeOAuthResults;  
	private String callbackID;
	
	public OAuthFinalize(String callbackID) throws TembooException {
		this.callbackID = callbackID;
		
		// Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
		TembooSession session = new TembooSession("faroskalin", "myFirstApp", "39e34c8c02b444879db847e11ebad224");

		FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

		// Get an InputSet object for the choreo
		FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

		// Set inputs
		finalizeOAuthInputs.set_CallbackID(this.callbackID);
		finalizeOAuthInputs.set_ClientSecret("a2hPHwlrQmVPJXTNRN69YOE1");
		finalizeOAuthInputs.set_ClientID("446983882758-t927vcfnmuchafclb7dcrapt2mu0devf.apps.googleusercontent.com");

		// Execute Choreo
		finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
	}

	/**
	 * Get the access token after a successful finalization OAuth process
	 * @return
	 */
	public String getAccessToken() {
		return finalizeOAuthResults.get_AccessToken();
	}
	
	/**
	 * Get the refresh token after a successful finalization OAuth process
	 * @return
	 */
	public String getRefreshToken() {
		return finalizeOAuthResults.get_RefreshToken();
	}
}
