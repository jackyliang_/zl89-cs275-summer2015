import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Hourly Weather Conditions Prediction using Weather Underground API.
 * Print a line for each hour that you find in the forecast, which 
 * should contain at least the date and hour, the current 
 * conditions (i.e., "Clear"), temperature, and 
 * relative humidity.
 * 
 * Libraries Needed:
 * https://repo1.maven.org/maven2/org/codeartisans/org.json/20131017/org.json-20131017.jar
 * 
 * Also require your Wunderground API key on request
 * @author Jacky Liang 13818060
 *
 */

public class Hourly
{
	private static String PHILADELPHIA;

	/**
	 * Read from a URL and return the HTML/text
	 * @param urlString
	 * @return
	 * @throws Exception
	 */
	public static String readUrl(String urlString) throws Exception {
	    BufferedReader reader = null;
	    try {
	        URL url = new URL(urlString);
	        reader = new BufferedReader(new InputStreamReader(url.openStream()));
	        StringBuffer buffer = new StringBuffer();
	        int read;
	        char[] chars = new char[1024];
	        while ((read = reader.read(chars)) != -1)
	            buffer.append(chars, 0, read); 

	        return buffer.toString();
	    } finally {
	        if (reader != null) {
	            reader.close();
	        }
	    }
	}

	/**
	 * Parse the Weather Underground hourly forecast and print the data
	 * @param url The supplied URL to store JSON data in
	 */
	public void parse(String url) {
		try
		{
			JSONObject json = new JSONObject(readUrl(url));
			
			// hourly_forecast is a JSON array
			JSONArray hourlyForecast = json.getJSONArray("hourly_forecast");
			
            // Iterate through each element within hourly_forecast
			for(int i = 0; i < hourlyForecast.length(); i++) {
				
				// Each object within hourly_forecast contains data for
				// each hourly forecast such as the condition,
				// temperature, and humidity
				JSONObject objects = hourlyForecast.getJSONObject(i);

				// Look within FCTTime to get the date
				JSONObject fc = objects.getJSONObject("FCTTIME");

                // Print a line for each hour that you find in the forecast, 
                // which should contain at least the date and hour, the 
                // current conditions (i.e., "Clear"), temperature,
                // and relative humidity.
				String dateAndTime = (String) fc.get("pretty");
				String condition = (String) objects.get("condition");
				double metricTemp = (double) objects.getJSONObject("temp").getDouble("metric");
				String humidity = (String) objects.get("humidity");
				
				// Print the results
				System.out.println("Report for " + dateAndTime);
				System.out.println("Condition: " + condition);
				System.out.println("The temperature will be " + metricTemp + "C");
				System.out.println("It will be " + humidity + "% humid");
				System.out.println();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void main(String[] args)
	{
		String API_KEY;

		// Ask user for key
		if(args.length < 1) {
			System.out.println("Enter key: ");
			Scanner in = new Scanner(System.in);
			API_KEY = in.nextLine();
		} else {
			API_KEY = args[0];
		}

		// Hourley forecast API URL
		PHILADELPHIA = "http://api.wunderground.com/api/" + API_KEY + "/hourly/q/PA/Philadelphia.json";
		
		// Parse and print the hourly predictions
		Hourly h = new Hourly();
		h.parse(PHILADELPHIA);
	}

}
