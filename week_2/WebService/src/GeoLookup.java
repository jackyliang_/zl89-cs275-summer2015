import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Scanner;

import org.json.JSONObject;

/**
 * GeoLookup using the Weather Underground API.
 * Obtain your current geographic information like city and state. 
 * Print out the user's zip code, city and state, or lat and long.
 * @author Jacky Liang 13818060
 *
 * Libraries Needed:
 * https://repo1.maven.org/maven2/org/codeartisans/org.json/20131017/org.json-20131017.jar
 * 
 * Also require your Wunderground API key on request
 */

public class GeoLookup
{
	/**
	 * City string constant
	 */
	private static String PHILADELPHIA; 

	/**
	 * URL to parse
	 */
	private String url;
	
	/**
	 * Information about the location of the geolookup results
	 */
	private int zipcode;
	private String city;
	private String state;
	
	/**
	 * Read from a URL and return the HTML/text
	 * @param urlString
	 * @return
	 * @throws Exception
	 */
	public static String readUrl(String urlString) throws Exception {
	    BufferedReader reader = null;
	    try {
	        URL url = new URL(urlString);
	        reader = new BufferedReader(new InputStreamReader(url.openStream()));
	        StringBuffer buffer = new StringBuffer();
	        int read;
	        char[] chars = new char[1024];
	        while ((read = reader.read(chars)) != -1)
	            buffer.append(chars, 0, read); 

	        return buffer.toString();
	    } finally {
	        if (reader != null) {
	            reader.close();
	        }
	    }
	}

	/**
	 * Parse the Weather Underground URL and store the JSON data
	 * @param url The supplied URL to store JSON data in
	 */
	public void parse(String url) {
		this.url = url;
		try
		{
			JSONObject json = new JSONObject(readUrl(this.url));
			JSONObject location = json.getJSONObject("location");
			this.zipcode = (int) location.getInt("zip"); 
			this.city = (String)location.get("city");
			this.state = (String)location.get("state");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Get the zip code
	 * @return The zip code in integer form
	 */
	public int getZipcode() {
		return this.zipcode;
	}

	/**
	 * Get the city
	 * @return Get the city name
	 */
	public String getCity() {
		return this.city;
	}
	
	/**
	 * Get the state
	 * @return
	 */
	public String getState() {
		return this.state;
	}

	public static void main(String[] args)
	{
		String API_KEY;

		// Ask user for key
		if(args.length < 1) {
			System.out.println("Enter key: ");
			Scanner in = new Scanner(System.in);
			API_KEY = in.nextLine();
		} else {
			API_KEY = args[0];
		}

		// Geolookup API URL
		PHILADELPHIA = "http://api.wunderground.com/api/" + API_KEY + "/geolookup/q/PA/Philadelphia.json";
		
		// Get data from Weather Underground and print it
		GeoLookup ws = new GeoLookup();
		ws.parse(PHILADELPHIA);
		System.out.println("Zipcode: " + ws.getZipcode());
		System.out.println("City: " + ws.getCity());
		System.out.println("State: " + ws.getState());
	}
}
