package com.drexel.jacky.tictactoe;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {
    int count = 0;
    int[] total1 = {0,0,0,0,0,0,0,0,0,0}; // Array which store the value for play 1
    int[] total2 = {0,0,0,0,0,0,0,0,0,0};// Array which store the value for player 2
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // A set of buttons
        final TextView winning = (TextView) findViewById(R.id.textViewXXX);
        final Button firstButton = (Button) findViewById(R.id.button1);
        final Button second_button = (Button) findViewById(R.id.button2);
        final Button third_button = (Button) findViewById(R.id.button3);
        final Button fourth_button = (Button) findViewById(R.id.button4);
        final Button fifth_button = (Button) findViewById(R.id.button5);
        final Button sixth_button = (Button) findViewById(R.id.button6);
        final Button seventh_button = (Button) findViewById(R.id.button7);
        final Button eighth_button = (Button) findViewById(R.id.button8);
        final Button nineth_button = (Button) findViewById(R.id.button9);

        //The listener for each button basically they are the same just different value
        firstButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((count % 2 == 0) && (firstButton.getText().length() > 1)) { // Do this to make sure double clicking wont count
                    firstButton.setText("X");
                    total1[1] = 1;
                    if (findwhowin(total1)) {
                        winning.setText("Player One Wins!");

                    }
                    count++;
                }
                // Check for double-counting
                if ((count % 2 == 1) && (firstButton.getText().length() > 1)) {
                    firstButton.setText("O");
                    total2[1] = 1;
                    if (findwhowin(total2)) {
                        winning.setText("Player Two Wins!");

                    }
                    count++;
                }
                if (count == 9)
                    winning.setText("Draw Game end!!!");


            }
        });
        second_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((count % 2 == 0)&& (second_button.getText().length() > 1)){
                    second_button.setText("X");
                    total1[2]=2;
                    if (findwhowin(total1)) {
                        winning.setText("Player One Wins!");
                    }
                    count++;
                }
                if((count % 2 == 1)&& (second_button.getText().length() > 1)){
                    second_button.setText("O");
                    total2[2]=2;
                    if (findwhowin(total2)) {
                        winning.setText("Player Two Wins!");
                    }
                    count++;
                }
                if(count == 9)
                    winning.setText("The game ended in a draw!");
            }
        });
        third_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((count % 2 == 0)&& (third_button.getText().length() > 1)){
                    third_button.setText("X");
                    total1[3]= 3;
                    if (findwhowin(total1)) {
                        winning.setText("Player One Wins!");
                    }
                    count++;
                }
                if((count % 2 == 1)&& (third_button.getText().length() > 1)){
                    third_button.setText("O");
                    total2[3]=3;
                    if (findwhowin(total2)) {
                        winning.setText("Player Two Wins!");
                    }
                    count++;
                }
                if(count == 9)
                    winning.setText("The game ended in a draw!");
            }
        });
        fourth_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((count % 2 == 0)&& (fourth_button.getText().length() > 1)){
                    fourth_button.setText("X");
                    total1[4] = 4;
                    if (findwhowin(total1)) {
                        winning.setText("Player One Wins!");

                    }
                    count++;
                }
                if((count % 2 ==1 )&& (fourth_button.getText().length() > 1)){
                    fourth_button.setText("O");
                    total2[4]=4;
                    if (findwhowin(total2)) {
                        winning.setText("Player Two Wins!");

                    }
                    count++;
                }
                if(count == 9)
                    winning.setText("The game ended in a draw!");
            }
        });
        fifth_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((count % 2 == 0)&& (fifth_button.getText().length() > 1)){
                    fifth_button.setText("X");
                    total1[5] = 5;
                    if (findwhowin(total1)) {
                        winning.setText("Player One Wins!");

                    }
                    count++;
                }
                if((count % 2 == 1)&& (fifth_button.getText().length() > 1)){
                    fifth_button.setText("O");
                    total2[5]=5;
                    if (findwhowin(total2)) {
                        winning.setText("Player Two Wins!");

                    }
                    count++;
                }
                if(count == 9)
                    winning.setText("The game ended in a draw!");
            }
        });
        sixth_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((count % 2 == 0)&& (sixth_button.getText().length() > 1)){
                    sixth_button.setText("X");
                    total1[6]=6;
                    if (findwhowin(total1)) {
                        winning.setText("Player One Wins!");

                    }
                    count++;
                }
                if((count % 2 == 1)&& (sixth_button.getText().length() > 1)){
                    sixth_button.setText("O");
                    total2[6] = 6;
                    if (findwhowin(total2)) {
                        winning.setText("Player Two Wins!");

                    }
                    count++;
                }
                if(count == 9)
                    winning.setText("The game ended in a draw!");
            }
        });
        seventh_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((count % 2 == 0)&& (seventh_button.getText().length() > 1)){
                    seventh_button.setText("X");
                    total1[7] = 7;
                    if (findwhowin(total1)) {
                        winning.setText("Player One Wins!");

                    }
                    count++;
                }
                if((count % 2 == 1)&& (seventh_button.getText().length() > 1)){
                    seventh_button.setText("O");
                    total2[7]= 7;
                    if (findwhowin(total2)) {
                        winning.setText("Player Two Wins!");

                    }
                    count++;
                }
                if(count == 9)
                    winning.setText("The game ended in a draw!");

            }
        });
        eighth_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((count % 2 == 0)&& (eighth_button.getText().length() > 1)){
                    eighth_button.setText("X");
                    total1[8] = 8;
                    if (findwhowin(total1)) {
                        winning.setText("Player One Wins!");

                    }
                    count++;
                }
                if((count % 2 == 1)&& (eighth_button.getText().length() > 1)){
                    eighth_button.setText("O");
                    total2[8]=8;
                    if (findwhowin(total2)) {
                        winning.setText("Player Two Wins!");

                    }
                    count++;
                }
                if(count == 9)
                    winning.setText("The game ended in a draw!");
            }
        });
        nineth_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((count % 2 == 0)&& (nineth_button.getText().length() > 1)) {
                    nineth_button.setText("X");
                    total1[9] = 9;
                    if (findwhowin(total1)) {
                        winning.setText("Player One Wins!");

                    }
                    count++;
                }
                if((count % 2 == 1)&& (nineth_button.getText().length() > 1)) {
                    nineth_button.setText("O");
                    total2[9] = 9;
                    if (findwhowin(total2)) {
                        winning.setText("Player Two Wins!");

                    }
                    count++;
                }
                if(count == 9)
                    winning.setText("The game ended in a draw!");
            }
        });
    }
    // Loop through the multidimensional array to see who wins
    private boolean findwhowin(int[] c){
        for(int i = 0; i<c.length;i++){
            for(int j = i+1; j<c.length;j++){
                for(int z = j+1;z<c.length;z++){
                    if(c[i]+c[j]+c[z] == 15){
                        if((c[i] == 0)||(c[j]==0)||(c[z]==0))
                            continue;
                        return true;
                    }
                }
            }
        }
        return false;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
