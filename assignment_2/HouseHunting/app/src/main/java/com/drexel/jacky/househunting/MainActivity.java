package com.drexel.jacky.househunting;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.graphics.Camera;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


public class MainActivity extends ActionBarActivity {

    private static final LatLng DAVAO = new LatLng(7.0722, 125.6131);
    private GoogleMap map;

    private void setUpMap() {
        mMap.addMarker(new MarkerOptions().position(new LatLng(39.9534474,-75.1889307)).title("My Location"));
        mMap.addMarker(new MarkerOptions().position(new LatLng((Double.parseDouble(addressList.get(0).getLatitude())), Double.parseDouble(addressList.get(0).getLongitude()))).title(addressList.get(0).getMarker()));
        mMap.addMarker(new MarkerOptions().position(new LatLng((Double.parseDouble(addressList.get(1).getLatitude())), Double.parseDouble(addressList.get(1).getLongitude()))).title("3"));
        mMap.addMarker(new MarkerOptions().position(new LatLng((Double.parseDouble(addressList.get(2).getLatitude())), Double.parseDouble(addressList.get(2).getLongitude()))).title("4"));
        mMap.addMarker(new MarkerOptions().position(new LatLng((Double.parseDouble(addressList.get(3).getLatitude())), Double.parseDouble(addressList.get(3).getLongitude()))).title("2"));

        Polyline line = mMap.addPolyline(new PolylineOptions()
                .add(
                        new LatLng(39.9534474,-75.1889307),
                        new LatLng(Double.parseDouble(addressList.get(3).getLatitude()), Double.parseDouble(addressList.get(3).getLongitude())),
                        new LatLng((Double.parseDouble(addressList.get(1).getLatitude())), Double.parseDouble(addressList.get(1).getLongitude())),
                        new LatLng((Double.parseDouble(addressList.get(2).getLatitude())), Double.parseDouble(addressList.get(2).getLongitude())))
                .color(Color.BLACK));

    }

    protected ArrayList<Address> doInBackground(String... paramas) {

        try {
            Address A ;

            TembooSession session = new TembooSession("jackyliang", "exam", "lvnsg5H3BhNOjxbvEcNeuW230YcaxzFo");

            GeocodeByAddress geocodeByAddressChoreo = new GeocodeByAddress(session);

            for (int i =0; i<4; i++) {
                // Get an InputSet object for the choreo
                GeocodeByAddress.GeocodeByAddressInputSet geocodeByAddressInputs = geocodeByAddressChoreo.newInputSet();

                // Set inputs
                geocodeByAddressInputs.set_Address(placesToVisit.get(i));

                // Execute Choreo
                GeocodeByAddress.GeocodeByAddressResultSet geocodeByAddressResults = geocodeByAddressChoreo.execute(geocodeByAddressInputs);

                addressList.add(new Address(geocodeByAddressResults.get_Latitude(), geocodeByAddressResults.get_Longitude(),String.valueOf(i+1), placesToVisit.get(i)));
                System.out.println(addressList.get(i));

            }

            return addressList;

        } catch (Exception exception) {
            System.out.println(exception.toString());
        }

        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

        Marker davao = map.addMarker(new MarkerOptions().position(DAVAO).title("One House").snippet("A House You Are Interested In"));

        // zoom in the camera to Davao city
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(DAVAO, 15));

        // animate the zoom process
        map.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void addListenerOnButton() {

        searchB = (Button) findViewById(R.id.searchB);
        searchB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // retrieve Input information
                myhousing = new Train();
                dep = ((AutoCompleteTextView) findViewById(R.id.depInput)).getText().toString();
                des = ((AutoCompleteTextView) findViewById(R.id.desInput)).getText().toString();

                String a = "OnClickListener : " + dep + " " + des;

                Log.d("get input", a);
                Toast.makeText(MainActivity.this, a, Toast.LENGTH_SHORT).show();

                myhousing.setDep_address(dep);
                myhousing.setDes_address(des);

                housing_routing= new TrainRouting(myhousing);
                MyArrayAdapter myarrayadapter = new MyArrayAdapter(MainActivity.this,housing_routing.getData());
                listview.setAdapter(myarrayadapter);

                housing_routing.execute();
            }

        });

        showMapB = (Button) findViewById(R.id.showMapB);

        showMapB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setContentView(R.layout.activity_maps);
                map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
                        .getMap();
                if(map == null){
                    Toast.makeText(getApplicationContext(), "Map cannot found",
                            Toast.LENGTH_LONG).show();
                }else{

                    housingMapTask housingMap = new housingMapTask(getApplicationContext(), map);

                    housingMap.execute();
                    // Let the user see indoor maps where available.
                    map.setIndoorEnabled(true);

                    // Enable my-location stuff
                    map.setMyLocationEnabled(true);

                    // Move the "camera" (view position) to our center point.
                    map.moveCamera(CameraUpdateFactory.newLatLng(CENTER));
                    // Then animate the markers while the map is drawing,
                    // since you can't combine motion and zoom setting!
                    final int zoom = 10;
                    map.animateCamera(CameraUpdateFactory.zoomTo(zoom), 1500, null);
                }
            }

        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class AutoCompleteTask extends AsyncTask <Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {

                BufferedReader br = null;
                String urlCvs = "http://www.zillow.com/webservice/GetDeepSearchResults.htm?zws-id=X1-ZWz1a4anenndvv_6le3g";
                String line = "";
                int count = 0;

                URL _url = new URL(urlCvs);
                HttpURLConnection nRequest = (HttpURLConnection) _url.openConnection();
                nRequest.connect();

                br = new BufferedReader(new InputStreamReader((InputStream) nRequest.getContent()));

                while ((line = br.readLine()) != null) {
                    String[] place = line.split(",");

                    if (count != 0) {
                        placeList.add(place[1]);
                    }
                    count++;
                }
                Log.d(Integer.toString(placeList.size()), "placeLIst size");

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, placeList);
            AutoCompleteTextView sInput = (AutoCompleteTextView) findViewById(R.id.depInput);
            sInput.setAdapter(adapter1);
            Log.d("true", "autocomplete");
            ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, placeList);
            AutoCompleteTextView dInput = (AutoCompleteTextView) findViewById(R.id.desInput);
            dInput.setAdapter(adapter2);

        }
    }
}

