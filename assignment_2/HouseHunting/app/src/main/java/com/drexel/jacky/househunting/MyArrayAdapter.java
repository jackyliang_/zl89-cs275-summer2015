package com.example.hieubui.assignment2;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class MyArrayAdapter extends ArrayAdapter<Route> {

    private Context context;
    private ArrayList<Route> array_route;

    public MyArrayAdapter(Context context, ArrayList<Route> objects) {
        super(context,R.layout.list_item_layout, objects);
        this.context = context;
        array_route = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item_layout,null);
        }

        TextView textView1 = (TextView) convertView.findViewById(R.id.textView);
        TextView textView2 = (TextView) convertView.findViewById(R.id.textView2);
        TextView textView3 = (TextView) convertView.findViewById(R.id.textView3);
        TextView textView4 = (TextView) convertView.findViewById(R.id.textView4);
        textView1.setText(Integer.toString(array_route.get(position).getOrig_train()));
        textView2.setText(array_route.get(position).getOrig_dep_time());
        textView3.setText(array_route.get(position).getOrig_arr_time());
        textView4.setText(array_route.get(position).getOrig_delay());
        return convertView;
    }

}

