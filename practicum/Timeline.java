import com.temboo.Library.Twitter.Timelines.HomeTimeline;
import com.temboo.Library.Twitter.Timelines.HomeTimeline.HomeTimelineInputSet;
import com.temboo.Library.Twitter.Timelines.HomeTimeline.HomeTimelineResultSet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;


public class Timeline
{
	/**
	 * Twitter Application Constants
	 */
	public static final String CONSUMERSECRET = "rHqyjP5ks5iKVwDM4H1ckL1GKZAnBpTF89yVyi9UdfIV2YRvV0";
	public static final String CONSUMERKEY = "CooWQfK9OQD1T3CBBRRB9sCOw";
	private HomeTimelineResultSet homeTimelineResults;
	
	public Timeline(String accessToken, String accessTokenSecret, int numOfTweets) throws TembooException {
		
		TembooSession session = new TembooSession("faroskalin", "myFirstApp", "39e34c8c02b444879db847e11ebad224");
		HomeTimeline homeTimelineChoreo = new HomeTimeline(session);
	
		// Get an InputSet object for the choreo
		HomeTimelineInputSet homeTimelineInputs = homeTimelineChoreo.newInputSet();
	
		// Set inputs
		homeTimelineInputs.set_AccessToken(accessToken);
		homeTimelineInputs.set_AccessTokenSecret(accessTokenSecret);
		homeTimelineInputs.set_ConsumerSecret(CONSUMERSECRET);
		homeTimelineInputs.set_ConsumerKey(CONSUMERKEY);
		homeTimelineInputs.set_Count(numOfTweets);
		
		// Execute Choreo
		homeTimelineResults = homeTimelineChoreo.execute(homeTimelineInputs);
	}
	
	/**
	 * Get the Twitter timeline response
	 * @return
	 */
	public String getResponse() {
		return this.homeTimelineResults.get_Response();
	}
	
}
