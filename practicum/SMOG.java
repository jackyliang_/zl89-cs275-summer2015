import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


public class SMOG
{
	/**
	 * JSON array of the Twitter timeline
	 */
	private JsonArray items;
	
	/**
	 * Number of sentences
	 */
	private int numOfTweets;
	
	/**
	 * Parse my Twitter timeline and save the results
	 * as a JSON array
	 * @param myTimeline
	 */
	public SMOG(Timeline myTimeline, int numOfTweets) {
		this.numOfTweets = numOfTweets;
	    JsonParser jp = new JsonParser();
	    JsonElement root = jp.parse(myTimeline.getResponse());
	    this.items = root.getAsJsonArray();
	}

	/**
	 * SMOG grade of user is determine by:
	 * 
	 * grade = 1.0430 * sqrt(number_of_polysyllables * (30 / number_of_sentences)) + 3.1291 
	 * 
	 * where number_of_polysyllables is the number of words 
	 * which has more than three syllables. 
	 * number_of_sentences is the total number 
	 * of tweet from the user. The default 
	 * number of tweets is 30, and was set
	 * under the FinalizeOAuth Choreo
	 * @throws IOException
	 */
	public void getSMOGLevel() throws IOException{
	    List <String> id = new ArrayList<String>();
    	int numOfPollySyllables = 0;
	    for(int i = 0; i < items.size(); i++) {
	        JsonObject item = items.get(i).getAsJsonObject();
	        try{
	        	id.add(item.get("text").getAsString());
	        } catch (NullPointerException e){
	        	id.add("Null");
	        } finally {
	        	String a = id.get(i);	        	
	        	String[] b = a.split(" ");
	        	for(int j = 0; j < b.length; j++){
	        		String wordCountURL = "http://api.wordnik.com/v4/word.json/" 
	        				+ b[j] 
	        				+ "/hyphenation?useCanonical=false&limit=50&"
							+ "api_key=57d33c8e3ad03624e40070a501c0c8db97d26f888b2af7ac6";
	        		
	        		URL url = new URL(wordCountURL);
	        		
	        		HttpURLConnection request = (HttpURLConnection) url.openConnection();
	        		request.connect();
	            	JsonParser jpWord = new JsonParser();
	            	JsonElement rootWord = null; 
	            	try{
	            		if(request.getResponseCode() == 200) {
	            			rootWord = jpWord.parse(new InputStreamReader((InputStream) request.getContent()));
	            
	            			if(rootWord.isJsonArray()) {
	            				JsonArray rootObjWord = rootWord.getAsJsonArray();
	            				if(rootObjWord.size() >= 3) {
	            					numOfPollySyllables++;
	            				}
	            			}
	            		}
	            	} catch(Exception e) {
	            		j++;
	            	}
	        	}
	        	System.out.println("Parsing Tweet: " + a);
	        }
	    }
    	double grade = 1.0430 * Math.sqrt(numOfPollySyllables * (30 / numOfTweets)) + 3.1291; 	
    	System.out.println("Your SMOG level is: " + grade);
	} 

}
