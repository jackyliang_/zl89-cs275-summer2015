import com.temboo.Library.Twitter.OAuth.InitializeOAuth;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

public class OAuthInitialize
{
	/**
	 * OAuth Initialization object
	 */
	private InitializeOAuthResultSet initializeOAuthResults;

	/**
	 * The OAuth initialization variables of the authorization URL
	 * which will be generated on a successful initialization as
	 * well as the callback ID which stores the callback information
	 */
	private String iniAuthURL;
	private String iniCallbackID;
	private String iniOAuthTokenSecret;
	
	public OAuthInitialize() throws TembooException {
		TembooSession session = new TembooSession("faroskalin", "myFirstApp", "39e34c8c02b444879db847e11ebad224");

		InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

		// Get an InputSet object for the choreo
		InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

		// Set credential to use for execution
		initializeOAuthInputs.setCredential("TwitterOAuth");

		// Set inputs

		// Execute Choreo
		initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);
	}

	/**
	 * Generate the authorization URL for OAuth initialization
	 * @return Google OAuth URL
	 */
	public String getAuthURL() {
		iniAuthURL = initializeOAuthResults.get_AuthorizationURL();
		return this.iniAuthURL;
	}

	/**
	 * Generate the callback ID for OAuth initialization
	 * @return Google Callback ID
	 */
	public String getCallbackID() {
		iniCallbackID = initializeOAuthResults.get_CallbackID();
		return this.iniCallbackID;
	} 

	public String getOAuthTokenSecret() {
		iniOAuthTokenSecret = initializeOAuthResults.get_OAuthTokenSecret();
		return this.iniOAuthTokenSecret;
	}

}
