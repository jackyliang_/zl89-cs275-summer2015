#!/usr/bin/env bash

rm *.class;
chmod 744 *;
javac -cp temboo.jar:gson.jar:. OAuth.java;
java -cp temboo.jar:gson.jar:. OAuth;
