import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Scanner;

import com.temboo.core.TembooException;

public class OAuth
{
	
	public static void main(String[] args) throws TembooException, IOException, URISyntaxException, InterruptedException
	{
		OAuthInitialize iniOAuth = new OAuthInitialize();
		String callbackID = iniOAuth.getCallbackID();
		String oAuthTokenSecret = iniOAuth.getOAuthTokenSecret();
		

		/**
		 * Open the browser to allow the user to authorize using
		 * Google Authentication
		 */
		System.out.println("Copy this URL and paste it to your browser:");
		System.out.println();
		System.out.println(iniOAuth.getAuthURL());
		System.out.println();

		/**
		 * Wait for user input before continuing to the finalize
		 * OAuth procedure
		 */
		System.out.println("Press any key after you've authorized to continue.");
		Scanner scanner = new Scanner(System.in);
		String devNull = scanner.nextLine();

		// Initialize the finalize authorization object
		OAuthFinalize finOAuth = new OAuthFinalize(callbackID, oAuthTokenSecret);

		System.out.println("Access Token: " + finOAuth.getAccessToken() + "( COPY THIS )");
		System.out.println("Access Token Secret: " + finOAuth.getAccessTokenSecret() + "( COPY THIS )");
		System.out.println("Screen Name " + finOAuth.getScreenName());
		System.out.println("User ID: " + finOAuth.getUserID());
		System.out.println();

		int numOfTweets = 30;
		
		// Get a number of tweets from my timeline (using a new OAuth identity)
		Timeline myTimeline = new Timeline(
				finOAuth.getAccessToken(), 
				finOAuth.getAccessTokenSecret(), 
				numOfTweets
		);
		
		//int numOfTweets = 30;
		//String accessToken = "2379421776-TdWD9upBFQRyQjacakpxhEKSkA3rX3QX2nnfUpD";
		//String accessTokenSecret = "EZS9YmmKfKbGJtXMjCMMEc09c9CpmVyoJ8tHAZCRRc4X7";
		//// Get a number of tweets from my timeline (using preserved tokens)
		//Timeline myTimeline = new Timeline(accessToken, accessTokenSecret, numOfTweets);

		// Calculate my SMOG level
		SMOG mySmogLevel = new SMOG(myTimeline, numOfTweets);
		mySmogLevel.getSMOGLevel();
	}
}
