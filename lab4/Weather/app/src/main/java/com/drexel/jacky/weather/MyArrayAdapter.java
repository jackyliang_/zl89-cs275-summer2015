package com.drexel.jacky.weather;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Jacky Liang
 */
public class MyArrayAdapter extends ArrayAdapter<Weather> {

    private Context context;
    private ArrayList<Weather> array_weather;
    private boolean b;
    private boolean c;

    public MyArrayAdapter(Context context, ArrayList<Weather> objects,boolean b) {
        super(context,R.layout.list_item_layout, objects);
        this.context = context;
        array_weather = objects;
        this.b = b;
        this.c = true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item_layout,null);
        }

        TextView textView1 = (TextView) convertView.findViewById(R.id.textView);
        TextView textView2 = (TextView) convertView.findViewById(R.id.textView2);
        TextView textView3 = (TextView) convertView.findViewById(R.id.textView3);
        TextView textView4 = (TextView) convertView.findViewById(R.id.textView4);
        textView1.setText(array_weather.get(position).getCity());
        textView2.setText(array_weather.get(position).getHuminity());
        textView3.setText(array_weather.get(position).getHour());
        textView4.setText(array_weather.get(position).getDate());
        if(b){
            ImageView imageView = (ImageView) convertView.findViewById(R.id.imageView);//Display Image
            new DownloadImageTask(imageView).execute(array_weather.get(position).getImage_url());
        }
        return convertView;
    }
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                //e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }

    }
}

