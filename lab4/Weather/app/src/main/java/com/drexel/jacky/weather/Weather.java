package com.drexel.jacky.weather;

/**
 * Created by loop on 8/9/15.
 */
public class Weather {
    private String city;
    private String hour;
    private String date;
    private String huminity;
    private String image_url;

    public Weather(String city, String hour,String date,String huminity,String image_url){
        this.city = city;
        this.hour = hour;
        this.date = date;
        this.huminity = huminity;
        this.image_url = image_url;
    }
    public Weather(){}

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getHuminity() {
        return huminity;
    }

    public void setHuminity(String huminity) {
        this.huminity = huminity;
    }
}

