package com.drexel.jacky.weather;

/**
 * Created by loop on 8/9/15.
 */
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by Jacky Liang
 */
public class Sql_Open_Helper extends SQLiteOpenHelper {
    String CREATE_WEATHER_TABLE = "CREATE TABLE IF NOT EXISTS " + "Weather_Table" + "("
            + "Temp" + " TEXT," + "Hour" + " TEXT,"
            + "Date" + " TEXT," + "City" + " TEXT"+")";

    public Sql_Open_Helper(Context context) {
        super(context, "WEATHER-TABLE", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_WEATHER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS Weather_table");
        db.execSQL(CREATE_WEATHER_TABLE);
    }
    public void remake(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS Weather_table");
        db.execSQL(CREATE_WEATHER_TABLE);
    }
    public int getHour(SQLiteDatabase db){
        String query = "SELECT * FROM Weather_Table LIMIT 1";
        Cursor cursor = db.rawQuery(query, null);
        int foo = 25;
        if(cursor.moveToFirst()) {
            int hour = Integer.parseInt(cursor.getString(1));
            String date = cursor.getString(2);
            return hour;
        }

        else
            return foo;
    }
    public ArrayList<Weather> addWeatherToList (SQLiteDatabase db) {
        String small_query = "SELECT * FROM Weather_Table";
        Cursor small_cursor = db.rawQuery(small_query, null);
        ArrayList<Weather> weather_list = new ArrayList<Weather>();
        if (small_cursor.moveToFirst()) {
            do {
                Weather weather = new Weather();
                weather.setHuminity(small_cursor.getString(0));
                weather.setHour(small_cursor.getString(1));
                weather.setDate(small_cursor.getString(2));
                weather.setCity(small_cursor.getString(3));
                // Adding weather to list
                weather_list.add(weather);
            } while (small_cursor.moveToNext());
        }
        return weather_list;
    }
}
