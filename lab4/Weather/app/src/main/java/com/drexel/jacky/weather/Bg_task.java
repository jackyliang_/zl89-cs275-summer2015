package com.drexel.jacky.weather;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Jacky Liang
 */
public class Bg_task extends AsyncTask<Void, Void, Void> {

    ArrayList<Weather> data;
    boolean c = true;
    public ArrayList<Weather> getData() {return data;}
    public Context context;
    public Bg_task(Context context){
        data = new ArrayList<Weather>();
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... params){
        try{
            String key= "bf915a66cd91c078";

            String sURL = "http://api.wunderground.com/api/bf915a66cd91c078/geolookup/q/autoip.json";

            // Connect to the URL
            URL url = new URL(sURL);
            HttpURLConnection request = (HttpURLConnection) url.openConnection();
            request.connect();


            // Convert to a JSON object to print data
            JsonParser jp = new JsonParser();
            JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
            JsonObject rootobj = root.getAsJsonObject(); // may be Json Array if it's an array, or other type if a primitive



            // Get some data elements and print them)
            String city = rootobj.get("location").getAsJsonObject().get("city").getAsString();
            String sURL1 = "http://api.wunderground.com/api/" + key + "/hourly/q/PA/"+city+ ".json";
            URL url1 = new URL(sURL1);
            HttpURLConnection request1 = (HttpURLConnection) url1.openConnection();
            request1.connect();
            JsonParser jp1 = new JsonParser();
            JsonElement root1 = jp1.parse(new InputStreamReader((InputStream) request1.getContent()));
            JsonObject rootobj1 = root1.getAsJsonObject(); // may be Json Array if it's an array, or other type if a primitive

            String hour;
            String date;
            String humidity;
            String image_url;
            int l = rootobj1.get("hourly_forecast").getAsJsonArray().size();
            for (int i = 0; i < l; i++){
                hour = rootobj1.get("hourly_forecast").getAsJsonArray().get(i).getAsJsonObject().get("FCTTIME").getAsJsonObject().get("hour").getAsString();
                date = rootobj1.get("hourly_forecast").getAsJsonArray().get(i).getAsJsonObject().get("FCTTIME").getAsJsonObject().get("weekday_name").getAsString();
                humidity = rootobj1.get("hourly_forecast").getAsJsonArray().get(i).getAsJsonObject().get("temp").getAsJsonObject().get("english").getAsString();
                image_url = rootobj1.get("hourly_forecast").getAsJsonArray().get(i).getAsJsonObject().get("icon_url").getAsString();
                Weather weather = new Weather(city,hour,date,humidity,image_url);
                data.add(i, weather);

                //Insert data into the database
                Sql_Open_Helper sql_open_helper = new Sql_Open_Helper(this.context);
                SQLiteDatabase db = sql_open_helper.getWritableDatabase();
                if(this.c) {
                    sql_open_helper.remake(db);
                    this.c = false;
                }

                ContentValues values = new ContentValues();
                values.put("Temp", humidity);
                values.put("Hour", hour);
                values.put("Date", date);
                values.put("City", city);

                // Inserting Row
                db.insert("Weather_Table", null, values);
                db.close(); // Closing database connection
            }


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}

