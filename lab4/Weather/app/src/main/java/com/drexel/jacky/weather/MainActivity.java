package com.drexel.jacky.weather;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.util.Log;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Create weather list for handling database
        Sql_Open_Helper sql_open_helper = new Sql_Open_Helper(this);
        SQLiteDatabase db = sql_open_helper.getReadableDatabase();
        int hour = sql_open_helper.getHour(db); //Geting the last hour


        //Geting current hour for checking with in the last hour
        int current_hour = 14;
        //DateFormat Hhour = new SimpleDateFormat("kk:00 a");
        //int current_hour = Integer.parseInt(Hhour.format(Calendar.getInstance().getTime()).split(":",2)[0]);


        //Compare the last hour with the current hour
//        if(current_hour <= hour) {
            ArrayList<Weather> weather_list= sql_open_helper.addWeatherToList(db);
            MyArrayAdapter myArrayAdapter = new MyArrayAdapter(this, weather_list,false);
            ListView l = (ListView) findViewById(R.id.listView);
            l.setAdapter(myArrayAdapter);
//        } else {
            Bg_task bg_task = new Bg_task(this);
            MyArrayAdapter myArrayAdapter2 = new MyArrayAdapter(this, bg_task.getData(),true);
            ListView l2 = (ListView) findViewById(R.id.listView);
            l2.setAdapter(myArrayAdapter2);
            bg_task.execute();

//        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
