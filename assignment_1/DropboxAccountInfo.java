import com.temboo.Library.Dropbox.Account.AccountInfo;
import com.temboo.Library.Dropbox.Account.AccountInfo.AccountInfoInputSet;
import com.temboo.Library.Dropbox.Account.AccountInfo.AccountInfoResultSet;
import com.temboo.Library.Dropbox.FileOperations.CopyFileOrFolder;
import com.temboo.Library.Dropbox.FileOperations.CopyFileOrFolder.CopyFileOrFolderInputSet;
import com.temboo.Library.Dropbox.FileOperations.CopyFileOrFolder.CopyFileOrFolderResultSet;
import com.temboo.Library.Dropbox.FileOperations.CreateFolder;
import com.temboo.Library.Dropbox.FileOperations.CreateFolder.CreateFolderInputSet;
import com.temboo.Library.Dropbox.FileOperations.CreateFolder.CreateFolderResultSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile.GetFileInputSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile.GetFileResultSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.SearchFilesAndFolders;
import com.temboo.Library.Dropbox.FilesAndMetadata.SearchFilesAndFolders.SearchFilesAndFoldersInputSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.SearchFilesAndFolders.SearchFilesAndFoldersResultSet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

/**
 * This class contains a number of Dropbox API calls including
 * 1. Copy a file to and from a location
 * 2. Get Dropbox account information
 * 3. Search Dropbox for a file
 * 4. Get the contents of a file
 * @author Jacky Liang
 *
 */

public class DropboxAccountInfo
{
	/**
	 * Dropbox file operation and account information objects
	 */
	AccountInfoResultSet accountInfoResults;
	SearchFilesAndFoldersResultSet searchFilesAndFoldersResults;
	CopyFileOrFolderResultSet copyFileOrFolderResults;
	GetFileResultSet getFileResults;
	
	/**
	 * OAuth tokens
	 */
	private String accessToken;
	private String appSecret;
	private String accessTokenSecret;
	private String appKey;

	/**
	 * TembooSession object (previously instantiated)
	 */
	private TembooSession session;

	public DropboxAccountInfo(
			String accessToken,
			String appSecret,
			String accessTokenSecret,
			String appKey
	) throws TembooException
	{
		/**
		 * Save the OAuth tokens
		 */
		this.accessToken = accessToken;
		this.appSecret = appSecret;
		this.accessTokenSecret = accessTokenSecret;
		this.appKey = appKey;
		
		// Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
		this.session = new TembooSession("faroskalin", "myFirstApp", "39e34c8c02b444879db847e11ebad224");
	}
	
	/**
	 * Loads Dropbox Account info
	 * @throws TembooException
	 */
	public void loadAccountInfo() throws TembooException {
		
		AccountInfo accountInfoChoreo = new AccountInfo(session);

		// Get an InputSet object for the choreo
		AccountInfoInputSet accountInfoInputs = accountInfoChoreo.newInputSet();

		// Set inputs
		accountInfoInputs.set_AccessToken(accessToken);
		accountInfoInputs.set_AppSecret(appSecret);
		accountInfoInputs.set_AccessTokenSecret(accessTokenSecret);
		accountInfoInputs.set_AppKey(appKey);

		// Execute Choreo
		accountInfoResults = accountInfoChoreo.execute(accountInfoInputs);
	} 

	/**
	 * Dropbox file search 
	 * @param fileName         The file name to search
	 * @throws TembooException
	 */
	public void loadFileSearch(String fileName) throws TembooException {
		SearchFilesAndFolders searchFilesAndFoldersChoreo = new SearchFilesAndFolders(session);

		// Get an InputSet object for the choreo
		SearchFilesAndFoldersInputSet searchFilesAndFoldersInputs = searchFilesAndFoldersChoreo.newInputSet();

		// Set inputs
		searchFilesAndFoldersInputs.set_AppSecret(appSecret);
		searchFilesAndFoldersInputs.set_AccessToken(accessToken);
		searchFilesAndFoldersInputs.set_Query(fileName);
		searchFilesAndFoldersInputs.set_AccessTokenSecret(accessTokenSecret);
		searchFilesAndFoldersInputs.set_AppKey(appKey);

		// Execute Choreo
		searchFilesAndFoldersResults = searchFilesAndFoldersChoreo.execute(searchFilesAndFoldersInputs);
	}

	/**
	 * Get the results of the file search
	 * @return The JSON response of the file search
	 */
	public String getFileResults() {
		return searchFilesAndFoldersResults.get_Response();
	}

	/**
	 * Loads object to move file from and to a location
	 * @param fromPath         The from path
	 * @param toPath           The to path
	 * @throws TembooException
	 */
	public void moveFile(String fromPath, String toPath) throws TembooException{
		CopyFileOrFolder copyFileOrFolderChoreo = new CopyFileOrFolder(session);

		// Get an InputSet object for the choreo
		CopyFileOrFolderInputSet copyFileOrFolderInputs = copyFileOrFolderChoreo.newInputSet();

		// Set inputs
		copyFileOrFolderInputs.set_AppSecret(appSecret);
		copyFileOrFolderInputs.set_AccessToken(accessToken);
		copyFileOrFolderInputs.set_AccessTokenSecret(accessTokenSecret);
		copyFileOrFolderInputs.set_AppKey(appKey);

		copyFileOrFolderInputs.set_FromPath(fromPath);
		copyFileOrFolderInputs.set_ToPath(toPath);

		// Execute Choreo
		copyFileOrFolderResults = copyFileOrFolderChoreo.execute(copyFileOrFolderInputs);
	}

	/**
	 * Load objects to get file contents
	 * @param fileName         File name
	 * @param encode           Determine whether or not to base64 encode the file contents
	 * @throws TembooException
	 */
	public void loadFileContent(String fileName, Boolean encode) throws TembooException {
		GetFile getFileChoreo = new GetFile(session);

		// Get an InputSet object for the choreo
		GetFileInputSet getFileInputs = getFileChoreo.newInputSet();

		// Set inputs
		getFileInputs.set_AccessToken(accessToken);
		getFileInputs.set_AppSecret(appSecret);
		getFileInputs.set_AccessTokenSecret(accessTokenSecret);
		getFileInputs.set_AppKey(appKey);
		getFileInputs.set_Path(fileName);
		getFileInputs.set_EncodeFileContent(encode);

		// Execute Choreo
		getFileResults = getFileChoreo.execute(getFileInputs);
	}
	
	/**
	 * Get the contents of a specific file
	 * @return
	 */
	public String getFileContent() {
		return getFileResults.get_Response();
	} 

	/**
	 * Get the response after moving the file
	 * @return
	 */
	public String getMoveFileResponse() {
		return copyFileOrFolderResults.get_Response();
	}

	/**
	 * Get account information from Dropbox
	 * @return
	 */
	public String getAccountInfoResponse() {
		return accountInfoResults.get_Response();
	}
}
