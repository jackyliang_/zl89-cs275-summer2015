import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import org.apache.commons.codec.binary.Base64;

import com.temboo.core.TembooException;

/**
 * This class is the entry point to the Dropbox
 * Mover application that takes in a file called "__list"
 * and copies files to and front the specified locations
 * 
 * How it works:
 * 
 * 1. Authorize the user (if the tokens haven't expired)
 * 2. Parse the contents of __list to determine which files
 *    to copy and to where
 * 3. Copy the files 
 * 
 * Requires the Temboo Java SDK
 * 
 * @author Jacky Liang
 *
 */

public class DropboxMover
{
	/**
	 * Jacky's Dropbox application App Key and App Secret
	 * DO NOT CHANGE
	 */
	private static final String APPKEY = "zddok2t81wwkmah";
	private static final String APPSECRET = "4ijzxwymu8bspcr";

	/**
	 * Dropbox directory constants
	 */
	private static final String MOVE = "/move/";

	/**
	 * Newline and Space characters
	 */
	private static final String NEWLINE = "\n";
	private static final String SPACE = " ";

	/**
	 * With an input of a multi-line input string, convert the string
	 * into a hashmap where the key is the file name and the value
	 * is the destination file path
	 * For example:
	 * Key = "A.txt"
	 * Value = "/home/wmm72/"
	 * @param input Multiline string input i.e. from __list
	 */
	public static Map<String, String> getFileAndPath(String input){
		// Split new line and save each line to array
		String[] splitArr = input.split(NEWLINE);
		
		final Map<String, String> map = new HashMap<String, String>();
		
		// Split each line on space and save the first index
		// as the key (the file name) and the second index
		// as the value (destination dir)
		for(int i = 0; i < splitArr.length; i++){
			splitArr[i].split(SPACE);
			String[] file = splitArr[i].split(SPACE);
            map.put(file[0], file[1]);
		}
		return map;
	}
	
	/**
	 * Save the file to a local directory
	 * @param fileContent The base64 encoded string
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public static void save(
			String fileContent, 
			String toPath, 
			String toPathWithFileName
	) throws 
		FileNotFoundException, 
		IOException
	{
		// Decode the string file content
		byte[] data = Base64.decodeBase64(fileContent);

		// Make directory if it doesn't exist 
		// mkdir will handle existence check 
		// gracefully
		File directory = new File(toPath);
		directory.mkdirs();

		// Write it to the path with the full name
		try (OutputStream stream = new FileOutputStream(toPathWithFileName)) {
		    stream.write(data);
		}
	} 

	/**
	 * Iterate through the hash map, convert the items
	 * to toPaths and fromPaths, and copy them to 
	 * their new paths
	 * @param map
	 * @throws TembooException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public static void saveFilesToNewDir(
			Map<String, String> map, 
			DropboxAccountInfo myInfo
	) throws 
		TembooException, 
		FileNotFoundException, 
		IOException
	{
		// Iterate through the hash map
		for (Entry<String, String> entry : map.entrySet()) {
			// Assign the key as the from-path
			String fromPath = MOVE + entry.getKey();
		
			// File path
			String toPath = entry.getValue();
			
			// Assign the value as the to-path
			String toPathWithFile = entry.getValue() + "/" + entry.getKey();
			
			// Print a little message
		    System.out.println("Moving '" + fromPath + "' to '" + toPathWithFile + "'");
		    
		    // Load the base64 encoded file contents and save it to the
		    // specified location
			// Set base64encoding to false
			myInfo.loadFileContent(fromPath, true);
			save(myInfo.getFileContent(), toPath, toPathWithFile);
		}
	}
	
	public static void main(String[] args) 
			throws 
				TembooException, 
				IOException, 
				URISyntaxException, 
				InterruptedException
	{
		/**
		 *            USAGE INSTRUCTIONS
		 * Change to `dev` if tokens have not expired
		 * Change to `prod` if tokens expired
		 */
		String env = "prod";

		if(env.equals("dev")) {

			/* 
			 * Replace with newest access token and 
			 * access token secret
			 */
			String AccessToken = "feix7kydldgce1wp";
			String AccessTokenSecret = "4epaf193ko28qhd";
			
			/**
			 * Spawn a new Dropbox file system object
			 */
			DropboxAccountInfo myInfo = new DropboxAccountInfo(
					AccessToken, 
					APPSECRET,
					AccessTokenSecret, 
					APPKEY
			);
			
			/**
			 * Load the file contents from /move/__file
			 * Set base64encoding to false
			 */
			myInfo.loadFileContent("/move/__list", false);
			
			/**
			 * Save the contents of __file
			 */
			String __file = myInfo.getFileContent();
			
			/**
			 * Iterate through each line of __file and
			 * copy the key (which is the file name)
			 * and the value (the destination dir)
			 */
			saveFilesToNewDir(getFileAndPath(__file), myInfo);

		} else if(env.equals("prod")) {
			OAuthInitialize iniOAuth = new OAuthInitialize();
			String callbackID = iniOAuth.getCallbackID();
			String oAuthTokenSecret = iniOAuth.getOAuthTokenSecret();

			/**
			 * Open the browser to allow the user to authorize using
			 * Google Authentication
			 */
			System.out.println("Copy this URL and paste it to your browser:");
			System.out.println();
			System.out.println(iniOAuth.getAuthURL());
			System.out.println();

			/**
			 * Wait for user input before continuing to the finalize
			 * OAuth procedure
			 */
			System.out.println("Press any key after you've authorized to continue.");
			Scanner scanner = new Scanner(System.in);
			String devNull = scanner.nextLine();

			// Initialize the finalize authorization object
			OAuthFinalize finOAuth = new OAuthFinalize(callbackID, oAuthTokenSecret);
			
			System.out.println("Access Token: " + finOAuth.getAccessToken() + "  (COPY THIS)");
			System.out.println("Access Token Secret: " + finOAuth.getAccessTokenSecret() + "  (COPY THIS)");
			System.out.println();

			/**
			 * Spawn a new Dropbox file system object
			 */
			DropboxAccountInfo myInfo = new DropboxAccountInfo(
					finOAuth.getAccessToken(), 
					APPSECRET,
					finOAuth.getAccessTokenSecret(), 
					APPKEY
			);
			
			/**
			 * Load the file contents from /move/__file
			 * Set base64encoding to false
			 */
			myInfo.loadFileContent("/move/__list", false);
			
			/**
			 * Save the contents of __file
			 */
			String __file = myInfo.getFileContent();
			
			/**
			 * Iterate through each line of __file and
			 * copy the key (which is the file name)
			 * to the value (the destination dir)
			 */
			saveFilesToNewDir(getFileAndPath(__file), myInfo);
		}

	}
}
