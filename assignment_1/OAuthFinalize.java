import java.util.Scanner;

import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;


public class OAuthFinalize
{
	private FinalizeOAuthResultSet finalizeOAuthResults;  
	private String callbackID;
	private String oAuthTokenSecret;
	
	public OAuthFinalize(String callbackID, String oAuthTokenSecret) throws TembooException {
		this.callbackID = callbackID;
		this.oAuthTokenSecret = oAuthTokenSecret;
		
		// Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
		TembooSession session = new TembooSession("faroskalin", "myFirstApp", "39e34c8c02b444879db847e11ebad224");

		FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

		// Get an InputSet object for the choreo
		FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

		// Set credential to use for execution
		finalizeOAuthInputs.setCredential("DropboxOAuth");

		// Set inputs
		finalizeOAuthInputs.set_CallbackID(this.callbackID);
		finalizeOAuthInputs.set_OAuthTokenSecret(this.oAuthTokenSecret);


		// Execute Choreo
		finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
	}

	/**
	 * Get the access token after a successful finalization OAuth process
	 * @return
	 */
	public String getAccessToken() {
		return finalizeOAuthResults.get_AccessToken();
	}
	
	/**
	 * Get the refresh token after a successful finalization OAuth process
	 * @return
	 */
	public String getAccessTokenSecret() {
		return finalizeOAuthResults.get_AccessTokenSecret();
	}
}
